﻿using ApplicationSetting.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PublisherMS.Configurations.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace PublisherMS.Installers
{
    public class DbInstaller : IInstaller
    {
        public void InstallAssembly(IServiceCollection services, IConfiguration configuration)
        {
            //var connectionString = configuration.GetConnectionString(DbConfigurationConsts.SQLSERVER_DEFAULT_DATABASE_CONNECTIONSTRING) ??
            //           DbConfigurationConsts.SQLITE_DEFAULT_DATABASE_CONNECTIONSTRING;

            //var databaseProvider = configuration.GetConnectionString(DbConfigurationConsts.SQLSERVER_DEFAULT_DATABASE_PROVIDER);

            var migrationAssembly = typeof(Startup).GetTypeInfo().Assembly.GetName().Name;

            //if (string.IsNullOrWhiteSpace(databaseProvider))
            //    databaseProvider = DbConfigurationConsts.SQLITE_DEFAULT_DATABASE_PROVIDER;

            var connectionString = DbConfigurationConsts.SQLITE_DEFAULT_DATABASE_CONNECTIONSTRING;

            services.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseSqlite(connectionString, sql => sql.MigrationsAssembly(migrationAssembly));
                //if (databaseProvider.ToLower().Trim().Equals("sqlite"))
                //    options.UseSqlite(connectionString);
                //else if (databaseProvider.ToLower().Trim().Equals("sqlserver"))
                //{
                //    // only works in windows container
                //    options.UseSqlServer(connectionString, sql => sql.MigrationsAssembly(migrationAssembly));
                //}
                //else
                //    throw new Exception("Database provider unknown. Please check configuration");

            });

            services.AddDefaultIdentity<IdentityUser>()
                .AddRoles<IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>();
        }
    }
}
