﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PublisherMS.Mapper
{
    public static class ObjectApiMapper
    {
        static ObjectApiMapper()
        {
            Mapper = new MapperConfiguration(cfg => cfg.AddProfile<ObjectApiMapperProfile>())
                .CreateMapper();
        }

        internal static IMapper Mapper { get; }

        public static T ToApiModel<T>(this object source)
        {
            return Mapper.Map<T>(source);
        }
    }
}
