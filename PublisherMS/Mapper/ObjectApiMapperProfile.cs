﻿using AutoMapper;
using BusinessLogic.Dtos.Configuration;
using PublisherMS.Dtos;
using PublisherMS.Dtos.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PublisherMS.Mapper
{
    public class ObjectApiMapperProfile:Profile
    {
        public ObjectApiMapperProfile()
        {
            //CreateMap<PaperApiDto, PaperDto>(MemberList.Destination);
            CreateMap<ConferenceDto, ConferenceApiDto>(MemberList.Destination)
                .ForMember(x => x.Users,
                    opts => opts.MapFrom(src => src.Users.Select(
                        x => new AttendeeApiDto
                        {
                            UserId = x.UserId,
                            RoleId = x.RoleId
                        })));

            CreateMap<PapersDto, PapersApiDto>(MemberList.Destination)
            .ReverseMap();

            CreateMap<PaperDto, PaperApiDto>(MemberList.Destination)
                .ForMember(x => x.Feedbacks,
                    opt => opt.MapFrom(src => src.Feedbacks.Select(x => new FeedbackApiDto
                    {
                        Review = x.Review,
                        UserId = x.UserId
                    })));
        }
    }
}
