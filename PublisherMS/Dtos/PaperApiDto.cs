﻿using PublisherMS.Dtos.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PublisherMS.Dtos
{
    public class PaperApiDto:BaseApiDto
    {
        public string Name { get; set; }
        public string ContentType { get; set; }
        public int Size { get; set; }
        public DateTime Date { get; set; }
        public string User_Id { get; set; }
        public bool Status { get; set; }
        public List<FeedbackApiDto> Feedbacks { get; set; }
    }
}
