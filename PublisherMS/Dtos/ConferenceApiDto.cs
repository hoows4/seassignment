﻿using PublisherMS.Dtos.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PublisherMS.Dtos
{
    public class ConferenceApiDto
    {
        public ConferenceApiDto()
        {
            Users = new List<AttendeeApiDto>();
        }

        public string Title { get; set; }
        public string Abstract { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public int Paper_Id { get; set; }
        public int Room_Id { get; set; }
        public List<AttendeeApiDto> Users { get; set; }
    }
}
