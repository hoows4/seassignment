﻿using BusinessLogic.Dtos.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PublisherMS.Dtos
{
    public class PapersApiDto
    {
        public PapersApiDto()
        {
            Papers = new List<PaperDto>();
        }

        public int PageSize { get; set; }
        public int TotalCount { get; set; }
        public List<PaperDto> Papers { get; set; }
    }
}
