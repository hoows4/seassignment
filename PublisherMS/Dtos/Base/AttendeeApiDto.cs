﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PublisherMS.Dtos.Base
{
    public class AttendeeApiDto
    {
        public string UserId { get; set; }
        public int RoleId { get; set; }
    }
}
