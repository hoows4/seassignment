﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PublisherMS.Dtos.Base
{
    public class BaseApiDto
    {
        public int Id { get; set; }
    }
}
