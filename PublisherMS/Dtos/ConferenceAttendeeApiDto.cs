﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PublisherMS.Dtos
{
    public class ConferenceAttendeeApiDto
    {
        public ICollection<string> User { get; set; }
        public int ConferenceId { get; set; }
    }
}
