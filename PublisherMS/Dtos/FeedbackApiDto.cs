﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PublisherMS.Dtos
{
    public class FeedbackApiDto
    {
        public string Review { get; set; }
        public string UserId { get; set; }
    }
}
