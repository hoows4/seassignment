﻿using BusinessLogic.Dtos.Configuration;
using BusinessLogic.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PublisherMS.Configurations.Constants;
using PublisherMS.Dtos;
using PublisherMS.Mapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PublisherMS.Controllers
{
    [Produces("application/json")]
    public class ConferenceController:Controller
    {
        private readonly IConferenceService ConferenceService;

        public ConferenceController(IConferenceService conferenceService)
        {
            ConferenceService = conferenceService;
        }

        [Authorize(Policy = AuthorizationConsts.AdministrationPolicy)]
        [HttpPost("api/createConference")]
        public virtual async Task<IActionResult> CreateConferenceAsync([FromBody]ConferenceApiDto conf)
        {
            var conference = conf.ToApiModel<ConferenceDto>();

            var result = await ConferenceService.CreateConferenceAsync(conference);

            if (result)
            {
                return Ok(conference);
            }

            return NoContent();
        }

        [HttpGet("api/getConference/{confId}")]
        public virtual async Task<ActionResult<ConferenceApiDto>> GetConferenceAsync([FromRoute]int confId)
        {
            var result = await ConferenceService.GetConfrence(confId);
            var resultApi = result.ToApiModel<ConferenceApiDto>();

            return Ok(resultApi);
        }
    }
}
