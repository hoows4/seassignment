﻿using BusinessLogic.Dtos.Configuration;
using BusinessLogic.Extensions;
using BusinessLogic.Services.Interfaces;
using BusinessLogicShared.Common;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PublisherMS.Dtos;
using PublisherMS.Mapper;
using System;
using System.IO;
using System.Threading.Tasks;

namespace PublisherMS.Controllers
{
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class PaperController:Controller
    {
        private readonly IHostingEnvironment _environment;
        protected readonly IPaperService PaperService;

        public PaperController(IHostingEnvironment environment,IPaperService _paperService)
        {
            _environment = environment ?? throw new ArgumentNullException(nameof(environment));
            PaperService = _paperService;
        }

        [HttpPost("api/upload")]
        [Consumes("multipart/form-data")]
        public async Task<IActionResult> UploadFile(IFormFile file)
        {
            var uploads = Path.Combine(_environment.WebRootPath, "upload");
            var paper = new PaperApiDto();

            if (!TextFileHelper.CheckFileFormat(file)){
                return BadRequest(new { message = "File Format Invalid" });
            }

            if (file.Length > 0)
            {
                using (var fileStream = new FileStream(Path.Combine(uploads, file.FileName), FileMode.Create))
                {
                    await file.CopyToAsync(fileStream);
                    paper.ContentType = file.ContentType;
                    paper.Date = DateTime.UtcNow;
                    paper.Name = file.FileName;
                    paper.Size = (int)file.Length;
                    paper.User_Id = HttpContext.GetUserId();
                    var paperDto = paper.ToApiModel<PaperDto>();
                    await PaperService.UploadPaper(paperDto);
                }
            }
            return Ok(file);
        }

        [HttpGet("/api/getFiles")]
        public async Task<ActionResult<PapersApiDto>> GetFileListAsync(int size = 1,int pageSize = 10)
        {
            var papersDto = await PaperService.GetPaperListAsync(size, pageSize);
            var papersApiDto = papersDto.ToApiModel<PapersApiDto>();

            return Ok(papersApiDto);
        }

        [HttpGet("/api/getUserFiles")]
        public async Task<ActionResult<PapersApiDto>> GetUserFileListAsync(int size = 1, int pageSize = 10)
        {
            var papersDto = await PaperService.GetPaperListByIdAsync(HttpContext.GetUserId(), size, pageSize);
            var papersApiDto = papersDto.ToApiModel<PapersApiDto>();

            return Ok(papersApiDto);
        }

        [HttpGet("api/getPaper/{id}")]
        public async Task<ActionResult<PaperApiDto>> GetPaperById([FromRoute]int id)
        {
            var paperDto = await PaperService.GetPaperById(id);
            var paperApiDto = paperDto.ToApiModel<PaperApiDto>();

            return Ok(paperApiDto);
        }
    }
}
