﻿using BusinessLogic.Dtos.Configuration;
using BusinessLogic.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PublisherMS.Controllers
{
    public class IdentityController:Controller
    {
        private readonly IIdentityService IdentityService;

        public IdentityController(IIdentityService identityService)
        {
            IdentityService = identityService;
        }

        [HttpPost("api/user/register")]
        public async Task<ActionResult> Register(IdentityDto user)
        {
            var createUser = await IdentityService.RegisterAsync(user);

            return Ok(createUser);
        }

        [HttpPost("api/user/login")]
        public async Task<ActionResult> Login(IdentityDto user)
        {
            var login = await IdentityService.LoginAsync(user);

            return Ok(login);
        }

    }
}
