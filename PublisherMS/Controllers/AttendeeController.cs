﻿using BusinessLogic.Dtos.Configuration;
using BusinessLogic.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using PublisherMS.Dtos;
using PublisherMS.Mapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PublisherMS.Controllers
{
    public class AttendeeController:Controller
    {
        private readonly IAttendeeService AttendeeService;

        public AttendeeController(IAttendeeService attendeeService)
        {
            AttendeeService = attendeeService;
        }

        [HttpPost("api/addAttendee")]
        public virtual async Task<IActionResult> AddAttendeeToConference([FromBody]ConferenceAttendeeApiDto conAttendee)
        {
            var model = conAttendee.ToApiModel<ConferenceAttendeeDto>();
            var result = await AttendeeService.AddToConferenceAsync(model);

            if (result ==1)
            {
                return Ok();
            }

            return NoContent();
        }
    }
}
