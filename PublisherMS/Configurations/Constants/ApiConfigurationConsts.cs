﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PublisherMS.Configurations.Constants
{
    public class ApiConfigurationConsts
    {
        public const string ApiName = "PublisherAPI";

        public const string ApiVersionV1 = "v1";
    }
}
