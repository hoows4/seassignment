﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PublisherMS.Configurations.Constants
{
    public class AuthorizationConsts
    {
        public const string AdministrationPolicy = "RequireAdministratorRole";
        public const string RevieweristrationPolicy = "RequireAdministratorRole";

        public const string AdministrationRole = "Admin";
        public const string ReviewerRole = "Reviewer";

    }
}
