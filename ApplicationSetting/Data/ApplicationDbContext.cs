﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using EntityFramework.Entities;

namespace ApplicationSetting.Data
{
    public class ApplicationDbContext: IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options):base(options)
        {
        }

        public DbSet<Paper> Papers { get; set; }
        public DbSet<Room> Rooms { get; set; }
        public DbSet<Conference> Conferences { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<ConferenceAttendee>()
                .HasKey(x => new { x.ConferenceID, x.UserId });

            builder.Entity<Feedback>()
                .HasKey(x => new { x.PaperId, x.UserId });
        }
    }
}
