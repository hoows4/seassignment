﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogic.Dtos.Configuration
{
    public class AttendeeDto
    {
        public string UserId { get; set; }
        public int RoleId { get; set; }
    }
}
