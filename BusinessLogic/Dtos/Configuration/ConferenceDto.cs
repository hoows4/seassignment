﻿using BusinessLogic.Dtos.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogic.Dtos.Configuration
{
    public class ConferenceDto
    {
        public ConferenceDto()
        {
            Users = new List<AttendeeDto>();
        }

        public string Title { get; set; }
        public string Abstract { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public int Paper_Id { get; set; }
        public int Room_Id { get; set; }
        public List<AttendeeDto> Users { get; set; }
    }
}
