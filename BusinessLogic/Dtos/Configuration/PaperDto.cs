﻿using BusinessLogic.Dtos.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogic.Dtos.Configuration
{
    public class PaperDto:BaseDto
    {
        public string Name { get; set; }
        public int Size { get; set; }
        public DateTime Date { get; set; }
        public string ContentType { get; set; }
        public string User_Id { get; set; }
        public bool Status { get; set; }
        public List<FeedbackDto> Feedbacks { get; set; }
    }
}
