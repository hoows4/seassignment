﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogic.Dtos.Configuration
{
    public class FeedbackDto
    {
        public string Review { get; set; }
        public string UserId { get; set; }
    }
}
