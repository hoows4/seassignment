﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogic.Dtos.Configuration
{
    public class PapersDto
    {
        public PapersDto()
        {
            Papers = new List<PaperDto>();
        }

        public int PageSize { get; set; }
        public int TotalCount { get; set; }
        public List<PaperDto> Papers { get; set; }
    }
}
