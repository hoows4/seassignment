﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogic.Dtos.Configuration
{
    public class ConferenceAttendeeDto
    {
        public ICollection<AttendeeDto> User { get; set; }
        public int ConferenceId { get; set; }
    }
}
