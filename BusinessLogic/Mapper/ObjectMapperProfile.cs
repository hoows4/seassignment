﻿using AutoMapper;
using BusinessLogic.Dtos.Configuration;
using EntityFramework.Entities;
using EntityFrameworkExtension.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.Mapper
{
    public class ObjectMapperProfile:Profile
    {
        public ObjectMapperProfile()
        {
            //model to entity
            CreateMap<PaperDto, Paper>(MemberList.Destination);

            CreateMap<ConferenceDto, Conference>(MemberList.Destination);
            //entity to model

            CreateMap<Conference, ConferenceDto>(MemberList.Destination)
                .ForMember(x => x.Users,
                    opts => opts.MapFrom(src => src.ConferenceAttendees
                        .Select(x => new AttendeeDto
                        {
                            UserId = x.UserId,
                            RoleId = x.RoleId
                        })));

            CreateMap<AttendeeDto, ConferenceAttendee>(MemberList.Destination);

            //pagedList
            CreateMap<PagedList<Paper>, PapersDto>()
                .ForMember(x => x.Papers, opt => opt.MapFrom(src => src.Data));

            CreateMap<Paper, PaperDto>(MemberList.Destination)
                .ForMember(x => x.Feedbacks
                , opt => opt.MapFrom(src => src.Feedbacks
                     .Select(x => new FeedbackDto
                     {
                         Review = x.Review,
                         UserId = x.UserId
                     })));
        }
    }
}
