﻿using AutoMapper;
using BusinessLogic.Dtos.Configuration;
using EntityFramework.Entities;
using EntityFrameworkExtension.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogic.Mapper
{
    public static class ObjectMapper
    {
        static ObjectMapper()
        {
            Mapper = new MapperConfiguration(cfg => cfg.AddProfile<ObjectMapperProfile>())
                .CreateMapper();
        }
        internal static IMapper Mapper { get; }

        public static Paper ToEntity(this PaperDto paper)
        {
            return Mapper.Map<Paper>(paper);
        }

        public static PaperDto ToModel(this Paper paper)
        {
            return Mapper.Map<PaperDto>(paper);
        }

        public static Conference ToEntity(this ConferenceDto conf)
        {
            return Mapper.Map<Conference>(conf);
        }

        public static ConferenceAttendee ToEntity(this AttendeeDto product)
        {
            return Mapper.Map<ConferenceAttendee>(product);
        }

        public static ConferenceDto ToModel(this Conference conf)
        {
            return conf == null ? null: Mapper.Map<ConferenceDto>(conf);
        }

        public static PapersDto ToModel(this PagedList<Paper> product)
        {
            return Mapper.Map<PapersDto>(product);
        }

    }
}
