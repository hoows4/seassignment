﻿using BusinessLogic.Services.Interfaces;
using EntityFramework.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using BusinessLogic.Dtos.Configuration;
using System.Threading.Tasks;
using EntityFramework.Entities;
using BusinessLogic.Mapper;

namespace BusinessLogic.Services
{
    public class AttendeeService:IAttendeeService
    {
        protected readonly IAttendeeRepository attendeeRepository;

        public AttendeeService(IAttendeeRepository attendeeRepository)
        {
            this.attendeeRepository = attendeeRepository;
        }

        public virtual async Task<int> AddToConferenceAsync(ConferenceAttendeeDto conAttendee)
        {
            var users = new List<ConferenceAttendee>();
            foreach(var ca in conAttendee.User)
            {
                var user = ca.ToEntity();
                users.Add(user);
            }
            return await attendeeRepository.AddToConferenceAsync(conAttendee.ConferenceId, users);
        }
    }
}
