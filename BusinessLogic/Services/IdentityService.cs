﻿using BusinessLogic.Dtos.Configuration;
using BusinessLogic.Services.Interfaces;
using BusinessLogicShared.Common;
using BusinessLogicShared.Security;
using EntityFramework.Repositories.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Services
{

    public class IdentityService : IIdentityService
    {
        protected readonly IIdentityRepository IdentityRepository;
        private readonly JwtSettings _jwtSetting;
        private readonly TokenValidationParameters _tokenValidation;

        public IdentityService(IIdentityRepository identityRepository, JwtSettings jwtSetting, TokenValidationParameters tokenValidation)
        {
            IdentityRepository = identityRepository;
            _jwtSetting = jwtSetting;
            _tokenValidation = tokenValidation;
        }

        public virtual async Task<AuthenticationResult> LoginAsync(IdentityDto user)
        {
            var (identity, result) = await IdentityRepository.LoginAsync(user.Email, user.Password);

            if (!result)
            {
                Console.WriteLine(result);

                return new AuthenticationResult
                {
                    Errors = new[] { "Email or password invalid" }
                };
            }

            return await GenerateAuthenticationResultForUserAsync(identity);
        }

        public virtual async Task<AuthenticationResult> RegisterAsync(IdentityDto user)
        {
            var newUser = new IdentityUser
            {
                Email = user.Email,
                UserName = user.Email
            };

            var (identityResult, userId) = await IdentityRepository.RegisterAsync(newUser, user.Password);

            if (identityResult.Errors.Any())
            {
                return new AuthenticationResult
                {
                    Errors = identityResult.Errors.Select(x => x.Description)
                };
            };

            return await GenerateAuthenticationResultForUserAsync(newUser);
        }

        private async Task<AuthenticationResult> GenerateAuthenticationResultForUserAsync(IdentityUser user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_jwtSetting.Secret);
            Console.WriteLine(key);

            var claims = new List<Claim>
                {
                        new Claim(JwtRegisteredClaimNames.Sub,user.Email),
                        new Claim(JwtRegisteredClaimNames.Jti,Guid.NewGuid().ToString()),
                        new Claim(JwtRegisteredClaimNames.Email,user.Email),
                        new Claim("id",user.Id),
                    };

            var userClaims = await IdentityRepository.GetUserClaimAsync(user);
            claims.AddRange(userClaims);

            var userRoles = await IdentityRepository.GetUserRoles(user);
            foreach (var userRole in userRoles)
            {
                claims.Add(new Claim(ClaimTypes.Role, userRole));
                var role = await IdentityRepository.FindRoleByNameAsync(userRole);
                if (role == null) continue;
                var roleClaims = await IdentityRepository.GetRoleClaimsAsync(role);

                foreach (var roleClaim in roleClaims)
                {
                    if (claims.Contains(roleClaim))
                        continue;
                    claims.Add(roleClaim);
                }
            }

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.UtcNow.Add(_jwtSetting.TokenLifeTime),
                SigningCredentials =
            new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);

            return new AuthenticationResult
            {
                Success = true,
                Token = tokenHandler.WriteToken(token),
            };
        }
    }
   
}
