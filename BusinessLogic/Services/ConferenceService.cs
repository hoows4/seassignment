﻿using BusinessLogic.Services.Interfaces;
using EntityFramework.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using BusinessLogic.Dtos.Configuration;
using System.Threading.Tasks;
using BusinessLogic.Mapper;

namespace BusinessLogic.Services
{
    public class ConferenceService:IConferenceService
    {
        protected readonly IConferenceRepository ConferenceRepository;

        public ConferenceService(IConferenceRepository conferenceRepository)
        {
            ConferenceRepository = conferenceRepository;
        }

        public virtual async Task<bool> CreateConferenceAsync(ConferenceDto conf)
        {
            var conference = conf.ToEntity();
            var result = await ConferenceRepository.CreateConferenceAsync(conference);

            return result == 1 ?true:false;
        }

        public virtual async Task<ConferenceDto> GetConfrence(int conId)
        {
            var entity = await ConferenceRepository.GetConferenceAsync(conId);
            var model = entity.ToModel();

            return model;
        }
    }
}
