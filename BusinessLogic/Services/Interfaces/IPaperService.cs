﻿using BusinessLogic.Dtos.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Services.Interfaces
{
    public interface IPaperService
    {
        Task<int> UploadPaper(PaperDto paper);

        Task<PapersDto> GetPaperListAsync(int size = 1,int pageSize =1);

        Task<PapersDto> GetPaperListByIdAsync(string userId,int size = 1, int pageSize = 1);

        Task<PaperDto> GetPaperById(int id);
    }
}
