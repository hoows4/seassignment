﻿using BusinessLogic.Dtos.Configuration;
using BusinessLogicShared.Common;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Services.Interfaces
{
    public interface IIdentityService
    {
        Task<AuthenticationResult> RegisterAsync(IdentityDto user);
        Task<AuthenticationResult> LoginAsync(IdentityDto user);
    }
}
