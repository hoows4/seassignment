﻿using BusinessLogic.Dtos.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Services.Interfaces
{
    public interface IConferenceService
    {
        Task<bool>CreateConferenceAsync(ConferenceDto conf);

        Task<ConferenceDto> GetConfrence(int conId);
    }
}
