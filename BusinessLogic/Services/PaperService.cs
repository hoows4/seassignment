﻿using BusinessLogic.Services.Interfaces;
using EntityFramework.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using BusinessLogic.Dtos.Configuration;
using System.Threading.Tasks;
using BusinessLogic.Mapper;

namespace BusinessLogic.Services
{
    public class PaperService:IPaperService
    {
        protected readonly IPaperRepository PaperRepository;

        public PaperService(IPaperRepository paperRepository)
        {
            PaperRepository = paperRepository;
        }

        public virtual async Task<PaperDto> GetPaperById(int id)
        {
            var paper = await PaperRepository.GetPaperById(id);
            var paperModel = paper.ToModel();

            return paperModel;
        }

        public virtual async Task<PapersDto> GetPaperListAsync(int size = 1, int pageSize = 1)
        {
            var pagedList = await PaperRepository.GetPaperListAsync(size, pageSize);
            var papersDto = pagedList.ToModel();

            return papersDto;
        }

        public virtual async Task<PapersDto> GetPaperListByIdAsync(string userId,int size = 1, int pageSize = 1)
        {
            var pagedList = await PaperRepository.GetPaperListByIdAsync(userId,size, pageSize);
            var papersDto = pagedList.ToModel();

            return papersDto;
        }

        public virtual async Task<int> UploadPaper(PaperDto paper)
        {
            var paperEntity = paper.ToEntity();

            return await PaperRepository.AddFileAsync(paperEntity);
        }
    }
}
