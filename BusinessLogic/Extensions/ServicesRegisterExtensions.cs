﻿using BusinessLogic.Services;
using BusinessLogic.Services.Interfaces;
using EntityFramework.Repositories;
using EntityFramework.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogic.Extensions
{
    public static class ServicesRegisterExtensions
    {
        public static IServiceCollection AddServices<TDbContext>(this IServiceCollection services)
           where TDbContext : DbContext
        {
            //Repositories
            services.AddTransient<IPaperRepository, PaperRepository<TDbContext>>();
            services.AddTransient<IIdentityRepository, IdentityRepository<TDbContext>>();
            services.AddTransient <IConferenceRepository, ConferenceRepository<TDbContext>>();
            services.AddTransient<IAttendeeRepository, AttendeeRepository<TDbContext>>();

            //Services
            services.AddTransient<IPaperService, PaperService>();
            services.AddTransient<IIdentityService, IdentityService>();
            services.AddTransient<IConferenceService, ConferenceService>();
            services.AddTransient<IAttendeeService, AttendeeService>();

            return services;
        }
    }
}