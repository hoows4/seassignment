﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogicShared.ExceptionHandling
{
    public class ViewErrorMessage
    {
        public string ErrorKey { get; set; }
        public string ErrorMessage { get; set; }
    }
}
