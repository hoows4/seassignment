﻿using EntityFramework.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using EntityFramework.Entities;
using System.Threading.Tasks;
using EntityFrameworkExtension.Enums;
using System.Linq;

namespace EntityFramework.Repositories
{
    public class ConferenceRepository<TDbContext>:IConferenceRepository
        where TDbContext:DbContext
    {
        protected readonly TDbContext _appContext;
        public bool AutoSaveChanges { get; set; } = true;

        public ConferenceRepository(TDbContext appContext)
        {
            _appContext = appContext;
        }

        public virtual async Task<int> CreateConferenceAsync(Conference conf)
        {
            await _appContext.Set<Conference>().AddAsync(conf);

            return await AutoSaveChangesAsync();
        }

        private async Task<int> AutoSaveChangesAsync()
        {
            return AutoSaveChanges ? await _appContext.SaveChangesAsync() : (int)SavedStatus.WillBeSavedExplicitly;
        }

        public virtual async Task<Conference> GetConferenceAsync(int confId)
        {
            return await _appContext.Set<Conference>()
                .AsNoTracking()
                .Include(x => x.Paper)
                .Include(x => x.Room)
                .Include(x => x.ConferenceAttendees)
                .ThenInclude(xx => xx.User)
                .SingleOrDefaultAsync(x => x.Id == confId);
        }
    }
}
