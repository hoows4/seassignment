﻿using EntityFramework.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EntityFramework.Repositories.Interfaces
{
    public interface IConferenceRepository
    {
        Task<int> CreateConferenceAsync(Conference conf);

        Task<Conference> GetConferenceAsync(int confId);
    }
}
