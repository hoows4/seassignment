﻿using EntityFramework.Entities;
using EntityFrameworkExtension.Extensions;
using System.Threading.Tasks;

namespace EntityFramework.Repositories.Interfaces
{
    public interface IPaperRepository
    {
        Task<int> AddFileAsync(Paper paper);

        Task<PagedList<Paper>> GetPaperListAsync(int page = 1, int pageSize = 1);

        Task<PagedList<Paper>> GetPaperListByIdAsync(string userId,int page = 1, int pageSize = 1);

        Task<Paper> GetPaperById(int id);
    }
}
