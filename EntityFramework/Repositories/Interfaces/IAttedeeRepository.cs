﻿using EntityFramework.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EntityFramework.Repositories.Interfaces
{
    public interface IAttendeeRepository
    {
        Task<int> AddToConferenceAsync(int confId, ICollection<ConferenceAttendee> users);
    }
}
