﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace EntityFramework.Repositories.Interfaces
{
    public interface IIdentityRepository
    {
        Task<(IdentityResult identityResult, string userId)> RegisterAsync(IdentityUser user, string password);
        Task<(IdentityUser user, bool result)> LoginAsync(string email, string password);
        Task<IList<Claim>> GetUserClaimAsync(IdentityUser user);
        Task<IList<string>> GetUserRoles(IdentityUser user);
        Task<IdentityRole> FindRoleByNameAsync(string role);
        Task<IList<Claim>> GetRoleClaimsAsync(IdentityRole role);
    }
}
