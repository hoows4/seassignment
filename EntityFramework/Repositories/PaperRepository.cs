﻿using EntityFramework.Entities;
using EntityFramework.Repositories.Interfaces;
using EntityFrameworkExtension.Enums;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityFrameworkExtension.Extensions;

namespace EntityFramework.Repositories
{
    public class PaperRepository<TDbContext>:IPaperRepository
        where TDbContext:DbContext
    {
        protected readonly TDbContext AppDbContext;
        public bool AutoSaveChanges { get; set; } = true;

        public PaperRepository(TDbContext appDbContext)
        {
            AppDbContext = appDbContext;
        }

        public virtual async Task<int> AddFileAsync(Paper paper)
        {
            await AppDbContext.Set<Paper>().AddAsync(paper);
            return await AutoSaveChangesAsync();
        }



        private async Task<int> AutoSaveChangesAsync()
        {
            return AutoSaveChanges ? await AppDbContext.SaveChangesAsync() : (int)SavedStatus.WillBeSavedExplicitly;
        }

        public virtual async Task<PagedList<Paper>> GetPaperListAsync(int page = 1, int pageSize =1)
        {
            var pagedList = new PagedList<Paper>();

            var papers = await AppDbContext.Set<Paper>()
                .PageBy(x => x.Id,page,pageSize)
                .ToListAsync();

            pagedList.Data.AddRange(papers);
            pagedList.TotalCount = await AppDbContext.Set<Paper>().CountAsync();
            pagedList.PageSize = pageSize;

            return pagedList;
        }

        public virtual async Task<PagedList<Paper>> GetPaperListByIdAsync(string userId,int page = 1, int pageSize = 1)
        {
            var pagedList = new PagedList<Paper>();

            var papers = await AppDbContext.Set<Paper>().Where(x => x.User_Id == userId)
                .PageBy(x => x.Id, page, pageSize)
                .ToListAsync();

            pagedList.Data.AddRange(papers);
            pagedList.TotalCount = await AppDbContext.Set<Paper>().CountAsync();
            pagedList.PageSize = pageSize;

            return pagedList;
        }

        public virtual async Task<Paper> GetPaperById(int id)
        {
            var paper = await AppDbContext.Set<Paper>()
                .Include(x => x.Feedbacks)
                .Where(x => x.Id == id).SingleOrDefaultAsync();

            return paper;
        }
    }
}
