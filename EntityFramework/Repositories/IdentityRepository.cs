﻿using EntityFramework.Repositories.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace EntityFramework.Repositories
{
    public class IdentityRepository<TDbContext>:IIdentityRepository
        where TDbContext:DbContext
    {
        protected readonly UserManager<IdentityUser> _usermanager;
        protected readonly RoleManager<IdentityRole> _roleManager;
        protected readonly TDbContext _appDbContext;

        public IdentityRepository(UserManager<IdentityUser> usermanager, RoleManager<IdentityRole> roleManager, TDbContext appDbContext)
        {
            _usermanager = usermanager;
            _roleManager = roleManager;
            _appDbContext = appDbContext;
        }

        public virtual async Task<IList<Claim>> GetUserClaimAsync(IdentityUser user)
        {
            return await _usermanager.GetClaimsAsync(user);
        }

        public virtual async Task<IList<string>> GetUserRoles(IdentityUser user)
        {
            return await _usermanager.GetRolesAsync(user);
        }

        public virtual async Task<IdentityRole> FindRoleByNameAsync(string role)
        {
            return await _roleManager.FindByNameAsync(role);
        }

        public virtual async Task<IList<Claim>> GetRoleClaimsAsync(IdentityRole role)
        {
            return await _roleManager.GetClaimsAsync(role);
        }

        public virtual async Task<(IdentityResult identityResult, string userId)> RegisterAsync(IdentityUser user, string password)
        {
            var identityResult = await _usermanager.CreateAsync(user,password);

            return (identityResult, user.Id);
        }

        public virtual async Task<(IdentityUser user, bool result)> LoginAsync(string email,string password)
        {
            var user = await _usermanager.FindByEmailAsync(email);

            var result = await _usermanager.CheckPasswordAsync(user,password);

            return (user, result);
        }
    }
}
