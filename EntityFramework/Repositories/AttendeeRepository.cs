﻿using EntityFramework.Entities;
using EntityFramework.Repositories.Interfaces;
using EntityFrameworkExtension.Enums;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EntityFramework.Repositories
{
    public class AttendeeRepository<TDbContext>:IAttendeeRepository
        where TDbContext:DbContext
    {
        protected readonly TDbContext _appContext;
        public bool AutoSaveChanges { get; set; } = true;

        public AttendeeRepository(TDbContext appContext)
        {
            _appContext = appContext;
        }

        public virtual async Task<int> AddToConferenceAsync(int confId, ICollection<ConferenceAttendee> users)
        {
            foreach(var user in users)
            {
                var conferenceAttendee = new ConferenceAttendee
                {
                    ConferenceID = confId,
                    UserId = user.UserId,
                    RoleId = user.RoleId
                };

                await _appContext.Set<ConferenceAttendee>().AddRangeAsync(conferenceAttendee);
            }

            return await AutoSaveChangesAsync();
        }

        private async Task<int> AutoSaveChangesAsync()
        {
            return AutoSaveChanges ? await _appContext.SaveChangesAsync() : (int)SavedStatus.WillBeSavedExplicitly;
        }
    }
}
