﻿using EntityFramework.Entities.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace EntityFramework.Entities
{
    public class Conference:Entity
    {
        public Conference()
        {
            ConferenceAttendees = new List<ConferenceAttendee>();
        }

        public string Title { get; set; }
        public string Abstract { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }

        [ForeignKey(nameof(Paper))]
        public int Paper_Id { get; set; }
        public Paper Paper { get; set; }

        [ForeignKey(nameof(Room))]
        public int Room_Id { get; set; }
        public Room Room { get; set; }

        public virtual ICollection<ConferenceAttendee> ConferenceAttendees { get; set; }
    }
}
