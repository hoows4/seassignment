﻿using EntityFramework.Entities.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EntityFramework.Entities
{
    public class Room:Entity
    {
        [Required]
        public string Name { get; set; }
    }
}
