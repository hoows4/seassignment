﻿using EntityFramework.Entities.Base;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace EntityFramework.Entities
{
    public class Feedback:Entity
    {
        public string Review { get; set; }

        [ForeignKey(nameof(UserId))]
        public IdentityUser User { get; set; }
        public string UserId { get; set; }

        [ForeignKey(nameof(PaperId))]
        public Paper Paper { get; set; }
        public int PaperId { get; set; }
    }
}
