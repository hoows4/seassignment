﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace EntityFramework.Entities
{
    public class ConferenceAttendee
    {
        public int ConferenceID { get; set; }

        public Conference Conference { get; set; }

        public string UserId { get; set; }

        public IdentityUser User { get; set; }

        public int RoleId { get; set; }
    }
}
