﻿using System;
using System.Collections.Generic;
using System.Text;
using EntityFramework.Entities.Base;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EntityFramework.Entities
{
    public class Paper: Entity
    {
        public Paper()
        {
            Feedbacks = new List<Feedback>();
        }

        [Required,StringLength(80)]
        public string Name { get; set; }
        public int Size { get; set; }
        public DateTime Date { get; set; }
        public string ContentType { get; set; }
        public bool Status { get; set; } = false;

        [ForeignKey(nameof(User_Id))]
        public IdentityUser User { get; set; }
        public string User_Id { get; set; }

        public ICollection<Feedback> Feedbacks { get; set; }
    }
}
